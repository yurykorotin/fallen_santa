import 'package:fallen_santa/models/santa.dart';
import 'package:flame/flame.dart';
import 'package:flame/game.dart';
import 'package:flutter/material.dart';

class Game extends BaseGame{
  Game(this.dimenstions);

  Size dimenstions;
  bool _isOver = false;
  bool isOver() {
    return _isOver;
  }

  List<Santa> santaList = <Santa>[];
  Santa santa;

  @override
  void render(Canvas canvas) {
    super.render(canvas);

    /*TextPainter p = Flame.util
        .text(text, color: Colors.white, fontSize: 48.0, fontFamily: 'Halo');
    String over = "Game over";
    TextPainter overGame = Flame.util
        .text(over, color: Colors.white, fontSize: 48.0, fontFamily: 'Halo');
    gameOver
        ? overGame.paint(canvas, Offset(size.width / 5, size.height / 2))
        : p.paint(canvas,
        new Offset(size.width - p.width - 10, size.height - p.height - 10));*/
  }

  double creationTimer = 0.0;
  @override
  void update(double t) {
    if (santa == null) {
       santa = Santa(dimenstions, 5.2, 4.0);
       add(santa);
    }

    creationTimer += t;
    santa.modeDown();
    /*if (creationTimer >= 1) {
      creationTimer = 0.0;

      for (int i = 1; i <= 40 / 7; i++) {
        for (int j = 0; j < i; ++j) {
          var santa = new Santa(dimenstions, j.toDouble(), i.toDouble());
          santaList.add(santa);
          add(santa);
        }
      }
    }*/
    super.update(t);
  }
}