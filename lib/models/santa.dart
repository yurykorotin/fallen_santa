import 'package:flame/components/component.dart';
import 'package:flutter/material.dart';

class Santa extends SpriteComponent {
  static const SANTA_SIZE = 100.0;
  final STEP_FOR_FALLING = 16.2;

  Santa(this.dimenstions, this._positionX, this._positionY)
      : super.square(SANTA_SIZE, 'santa.png');

  Size dimenstions;

  double _positionX = 0.0;
  double _positionY = 0.0;

  bool _isFalling = true;
  bool _isRunning = false;
  bool explode = false;
  double maxY;

  void modeDown() {
    _positionY += STEP_FOR_FALLING;
  }

  void moveRight() {
    _positionX = 0.1;
  }

  @override
  void update(double t) {
    y += (t * STEP_FOR_FALLING);
  }

  @override
  void resize(Size size) {
    this.x = SANTA_SIZE + _positionX;
    this.y = SANTA_SIZE + _positionY;
    this.maxY = size.height;
  }

  @override
  bool destroy() {
    if (explode) {
      return true;
    }
    if (y == null || maxY == null) {
      return false;
    }
    bool destroy = y >= maxY + STEP_FOR_FALLING / 2;
    if (destroy) {
      print("Game over");
      return true;
    }
    return destroy;
  }
}